<?php

namespace JanuszWitrykus\CurrencyConverter\Model;

/**
 * Communicate with external api to get rates
 * Class Converter
 * @package JanuszWitrykus\CurrencyConverter\Model
 */
class Converter implements \JanuszWitrykus\CurrencyConverter\Api\ConverterInterface
{
    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;

    /**
     * @var string - return to communication if error, will be logged in browser console
     */
    private $errorMsg = 'We can\'t convert now, please contact our sales representative';
    /**
     * Convert constructor.
     * @param \Psr\Log\LoggerInterface $logger
     */
    public function __construct(\Psr\Log\LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * We return only two decimal places as showed in task.
     * @param float $convertFrom
     * @return float
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function convert($convertFrom)
    {
        $exchangeRate = $this->getExchangeRate();
        //this round is PROBABLY required, if not, return without it
        return round($exchangeRate * $convertFrom, 2);
    }

    /**
     * This is ready to be called for different currency pair but you want to have hardcoded
     * @param string $from
     * @param string $to
     * @return float - six places
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function getExchangeRate($from = 'RUB', $to = 'PLN')
    {
        $errInCommunication = false;
        try {
            $currPair = $from . '_' . $to;
            $client = new \GuzzleHttp\Client(
                [
                    //these values should go from config
                    'base_uri' => 'http://free.currencyconverterapi.com/api/v5/',
                    'timeout'  => 30,
                    'verify'   => false
                ]
            );
            /** @var \Psr\Http\Message\ResponseInterface $response */
            $response = $client->get("convert?q=$currPair&compact=y");
            if ($response->getStatusCode() == 200) {
                $data         = json_decode($response->getBody(), 1);
                if (array_key_exists($currPair, $data)) {
                    if (array_key_exists('val', $data[$currPair])) {
                        $ret = $data[$currPair]['val'];
                        if (is_float($ret)) {
                            return $ret;
                        } else {
                            $this->logger->critical(__METHOD__
                                . ' In response we have no float number: ' . $ret);
                            $errInCommunication = true;
                        }
                    } else {
                        $this->logger->critical(__METHOD__
                            . ' No value in response: ' . $currPair);
                        $errInCommunication = true;
                    }
                } else {
                    $this->logger->critical(__METHOD__
                        . ' No currency pair in response: ' . $currPair);
                    $errInCommunication = true;
                }
            } else {
                $this->logger->critical(__METHOD__
                    . ' Bad response from api: '
                    . $response->getReasonPhrase()
                    . ' code: '
                    . $response->getStatusCode());
                $errInCommunication = true;
            }
        } catch (\Throwable $t) {
            $this->logger->critical(__METHOD__ . ' ' . $t->getMessage());
            throw new \Magento\Framework\Exception\LocalizedException(
                __($this->errorMsg)
            );
        } finally {
            if ($errInCommunication) {
                throw new \Magento\Framework\Exception\LocalizedException(
                    __($this->errorMsg)
                );
            }
        }
    }
}
