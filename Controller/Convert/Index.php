<?php

namespace JanuszWitrykus\CurrencyConverter\Controller\Convert;

/**
 * Display page to enter values to convert
 * Class Index
 * @package JanuszWitrykus\CurrencyConverter\Controller\Convert
 */
class Index extends \Magento\Framework\App\Action\Action
{
    /**
     * @return \Magento\Framework\Controller\ResultInterface|\Magento\Framework\App\ResponseInterface
     */
    public function execute()
    {
        return $this->resultFactory->create(\Magento\Framework\Controller\ResultFactory::TYPE_PAGE);
    }
}
