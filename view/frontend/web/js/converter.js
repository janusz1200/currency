/**
 * Component to handle input
 */
define(
    [
        'ko',
        'jquery',
        'uiElement',
        'JanuszWitrykus_CurrencyConverter/js/action/get-exchange-rate'
    ], function(ko, $, Element, convertAction){
        'use strict';
        return Element.extend({
            //currency from
            currFrom: ko.observable(''),
            //currency after conversion
            currTo: ko.observable(''),
            //field to display all errors on frontend
            error: ko.observable(''),
            initialize: function() {
                this._super();
                return this;
            },
            initObservable: function() {
                this._super();
                var self = this;
                this.currFrom.subscribe(function(newValue){
                    self.checkCurrFromFormat(newValue);
                });
                return this;
            },
            //called when user type in currFrom field
            checkCurrFromFormat: function(newValue){
                this.hide('#currToContainer');
                //check here if number is comma or dot separated, decimal point allowed at start
                var regex = /^[0-9]*([\.,][0-9]{0,2})?$/;
                if (regex.test(newValue)) {
                    this.error('');
                    $('#button').prop('disabled', false);
                } else {
                    this.error('This is bad number');
                    $('#button').prop('disabled', true);
                }

            },
            //communicate with server to get exchange rate
            getRate: function() {
                var self = this;
                var cf = parseFloat(this.currFrom().replace(",","."));
                convertAction({'convertFrom': cf})
                    .done(function(response) {
                        self.getRateSuccess(response)
                    })
                    .fail(function(error) {
                        self.getRateFail(error)
                    });
            },
            getRateSuccess: function(response){
                this.error('');
                this.show('#currToContainer');
                this.currTo(response.toFixed(2).replace('.', ','));
            },
            getRateFail: function(error) {
                this.hide('#currToContainer');
                try {
                    //not sure if always error have responseText
                    console.log(error.responseText);
                }
                catch(error1) {
                    //intentionally blank
                }
                this.error('Conversion can\'t be done, we are sorry, please contact our sales representative');
            },
            hide: function(stringEl) {
                $(stringEl).hide();
            },
            show: function(stringEl) {
                $(stringEl).show();
            }
        });
    }
);
