/**
 * Component to prepare url to call web service on server
 */
define(
    [
        //we don't use 'Magento_Checkout/js/model/url-builder' because M2 uses there window.checkoutConfig
        //so we pass config by config
        'jquery'
    ],
    function ($) {
        "use strict";
        return {
            method: "rest",
            storeCode: '', //we set it in config - not in this project, here it is empty
            version: 'V1',
            serviceUrl: ':method/:storeCode/:version',

            createUrl: function (url, params) {
                var completeUrl = this.serviceUrl + url;
                return this.bindParams(completeUrl, params);
            },
            bindParams: function (url, params) {
                params.method = this.method;
                params.storeCode = this.storeCode;
                params.version = this.version;

                var urlParts = url.split("/");
                urlParts = urlParts.filter(Boolean);

                $.each(urlParts, function (key, part) {
                    part = part.replace(':', '');
                    if (params[part] != undefined) {
                        urlParts[key] = params[part];
                    }
                });
                return urlParts.join('/');
            },
            getUrlForConvert: function() {
                var url = '/janusz_witrykus/currency_converter/convert';
                return this.createUrl(url, {});
            }
        };
    }
);
