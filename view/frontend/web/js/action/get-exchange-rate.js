/**
 * Component to communicate between browser and server
 */
define(
    [
        'JanuszWitrykus_CurrencyConverter/js/model/url-manager',
        'mage/storage'
    ],
    function(urlManager, storage) {
        'use strict';
        return function(from) {
            return storage.post(
                urlManager.getUrlForConvert(),
                JSON.stringify(from)
            )
        }
    }
);
