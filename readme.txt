Module for converting currency from RUB into PLN
Use: http://free.currencyconverterapi.com/api/v5/convert?q=RUB_PLN&compact=y
It should be done as modal window opened from each place of the system,
but because in requirements we have: "Application must be responsive when user is waiting for the answer",
so I did as separate page to which navigation is from left upper corner.

